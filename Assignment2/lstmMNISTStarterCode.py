
import tensorflow as tf
from tensorflow.python.ops import rnn, rnn_cell
import numpy as np 

from tensorflow.examples.tutorials.mnist import input_data

mnist =input_data.read_data_sets("./MNIST_data", one_hot=True) #call mnist function

learningRate = 0.001
trainingIters = 100000
batchSize = 128
displayStep = 10

nInput = 28 #we want the input to take the 28 pixels
nSteps = 28 #every 28
nHidden = 64 #number of neurons for the RNN
nClasses = 10 #this is MNIST so you know

x = tf.placeholder('float', [None, nSteps, nInput])
y = tf.placeholder('float', [None, nClasses])

weights = {
    'out': tf.Variable(tf.random_normal([nHidden, nClasses]))
}

biases = {
    'out': tf.Variable(tf.random_normal([nClasses]))
}

cellType = 'gru'

def RNN(x, weights, biases):
    x = tf.transpose(x, [1,0,2])
    x = tf.reshape(x, [-1, nInput])
    x = tf.split(0, nSteps, x) #configuring so you can get it as needed for the 28 pixels

    if cellType == 'basic':
        lstmCell = rnn_cell.BasicRNNCell(nHidden)
    elif cellType == 'lstm':
        lstmCell = rnn_cell.LSTMCell(nHidden)
    elif cellType == 'gru':
        lstmCell = rnn_cell.GRUCell(nHidden)
    else:
        raise Exception("Bad cellType value: should be 'basic', 'gru' or 'lstm'!")

    #lstmCell = rnn_cell.BasicRNNCell(nHidden) #find which lstm to use in the documentation

    outputs, states = rnn.rnn(lstmCell, x, dtype=tf.float32) #for the rnn where to get the output and hidden state

    return tf.matmul(outputs[-1], weights['out']) + biases['out']

pred = RNN(x, weights, biases)

#optimization
#create the cost, optimization, evaluation, and accuracy
#for the cost softmax_cross_entropy_with_logits seems really good
cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(pred, y))
optimizer = tf.train.AdamOptimizer(learning_rate=learningRate).minimize(cost)

correctPred = tf.equal(tf.argmax(pred,1), tf.argmax(y,1))
accuracy = tf.reduce_mean(tf.cast(correctPred, tf.float32))

init = tf.initialize_all_variables()
dirName = './Results' + str(nHidden) + cellType + '/'

with tf.Session() as sess:
    sess.run(init)
    summary_writer = tf.train.SummaryWriter(dirName, sess.graph)
    # train_errors = []
    # train_loss = []
    # test_errors = []
    testData = mnist.test.images.reshape((-1, nSteps, nInput))
    testLabel = mnist.test.labels

    step = 1

    while step* batchSize < trainingIters:
        batchX, batchY = mnist.train.next_batch(batchSize) #mnist has a way to get the next batch
        batchX = batchX.reshape((batchSize, nSteps, nInput))
        #print ("It is running..!")

        sess.run(optimizer, feed_dict={x: batchX, y: batchY})

        if step % displayStep == 0:
            train_acc_summary = tf.scalar_summary("train_accuracy", accuracy)
            loss_summary = tf.scalar_summary("loss_accuracy", cost)
            test_acc_summary = tf.scalar_summary("test_accuracy", accuracy)

            acc = sess.run(accuracy, feed_dict={x: batchX, y: batchY})
            loss = sess.run(cost, feed_dict={x: batchX, y: batchY})

            train_acc_summary= sess.run(train_acc_summary,feed_dict={x: batchX, y: batchY})
            loss_summary = sess.run(loss_summary, feed_dict={x: batchX, y: batchY})


            # loss = sess.run(cost, feed_dict={x: batchX, y: batchY})
            test_acc = sess.run(accuracy, feed_dict={x: testData, y: testLabel})
            test_acc_summary = sess.run(test_acc_summary, feed_dict={x: testData, y: testLabel})


            print("Iter " + str(step*batchSize) + ", Minibatch Loss= " + \
                    "{:.6f}".format(loss) + ", Training Accuracy= " + \
                     "{:.5f}".format(acc))
            test_error = sess.run(accuracy, feed_dict={x: testData, y: testLabel})
            print("Testing Accuracy: " + str(test_error))

            summary_writer.add_summary(train_acc_summary, step)
            summary_writer.add_summary(loss_summary, step)
            summary_writer.add_summary(test_acc_summary, step)
            summary_writer.flush()


        step +=1
    print('Optimization finished')

    # testData = mnist.test.images.reshape((-1, nSteps, nInput))
    # testLabel = mnist.test.labels
    # print("Testing Accuracy:", \
    #     sess.run(accuracy, feed_dict={x: testData, y: testLabel}))

    # with open('RNN_errors_summary' + cellType + str(nHidden) + '.txt', 'a') as f:
    #     f.write("Run with parameters: \n")
    #     f.write("Number of Hidden neurons: " + str(nHidden) + '\n')
    #     f.write("Number of Iterations: " + str(trainingIters) + '\n')
    #     f.write("Type of RNN method: " + str(cellType) + '\n')
    #     f.write("Training errors: " + ",".join([str(tre) for tre in train_errors]) + "\n")
    #     f.write("Training loss: " + ",".join([str(trl) for trl in train_loss]) + "\n")
    #     f.write("Test errors: " + ",".join([str(test_error) for test_error in test_errors]) + '\n')
    #     f.write('\n\n')
