__author__ = 'Pankaj_Singh'
import numpy as np
from sklearn import datasets, linear_model
import matplotlib.pyplot as plt
from three_layer_neural_network import NeuralNetwork, generate_data
from basic_functions import actFun, diff_actFun

#####################################
#DeepNeruralNetwork Class Generation
#####################################

class DeepNeuralNetwork(NeuralNetwork):
    """
    This class builds and trains a multi-layer (deep) neural network.
    """
    weights = []
    biases = []

    def __init__(self, layer_count, layer_size, layer_funcs, reg_lambda=0.01, seed=0):
        '''
        :param layer_count: an integer representing the number of layers
        :param layer_size: a tuple describing the number of nodes/neurons in all the layers
        :param layer_funcs: a list containing the names of the activation functions for each layer
        :param reg_lambda: regularization term
        :param seed: random seed
        '''
        # info
        self.layer_count =layer_count
        self.layer_size = layer_size
        self.layer_funcs = layer_funcs
        self.reg_lambda=reg_lambda
        # weights initialization
        for (n1, n2) in zip(layer_size[:-1], layer_size[1:]):
            self.weights.append(np.random.randn(n1, n2) /np.sqrt(n1))
            self.biases.append(np.zeros((1, n2)))

        self.input_layer = []
        self.output_layer =[]
        self.all_delta=[]

        # collect derivatives with respect to weight matrices and biases
        self.dWs = []
        self.dBs = []


        # initialize random seed
        np.random.seed(seed)

    def feedforward(self, X):
        """ Determine the layer inputs"""
        self.input_layer = []
        self.output_layer = []
        for index in range(len(self.weights)):
            if index == 0:
                layerInput = X.dot(self.weights[0]) + self.biases[0]
            else:
                layerInput = self.output_layer[-1].dot(self.weights[index]) + self.biases[index]
            self.input_layer.append(layerInput)
            self.output_layer.append(actFun(layerInput, self.layer_funcs[index]))
        return self.input_layer, self.output_layer


    def backprop(self, X, y):
        num_examples = len(X)
        # softmax of the last input layer
        exp_scores = np.exp(self.input_layer[-1])
        self.probs = exp_scores / np.sum(exp_scores, axis=1, keepdims=True)
        delta = self.probs
        delta[range(num_examples), y] -= 1
        collect_deltas=[]
        reversed_inputs = list(reversed(self.input_layer))
        reversed_outputs = list(reversed(self.output_layer))
        reversed_weigths = list(reversed(self.weights))
        reversed_biases = list(reversed(self.biases))
        collect_deltas.append(delta)
        self.dWs.append(self.output_layer[-1].T.dot(delta))
        self.dBs.append(np.sum(delta, axis=0, keepdims=True))

        for index in range(len(self.weights)):
            if index == 0:
                dW = self.output_layer[-1].T.dot(delta)
                dB = np.sum(delta, axis=0, keepdims=True)
            else:
                derivative = diff_actFun(self.input_layer[index], type=self.layer_funcs[index])
            #     delta2 = delta3.dot(self.W2.T) * derivative
            #     dW1 = X.T.dot(delta2)
            #     db1 = np.sum(delta2, axis=0)
            #     layerInput = self.output_layer[-1].dot(self.weights[index]) + self.biases[index]
            # self.input_layer.append(layerInput)
            # self.output_layer.append(actFun(layerInput, self.layer_funcs[index]))
        return None




    # def calculate_loss(self, X, y):
    #     pass
    #
    # def fit_model(self, X, y, epsilon=0.01, num_passes=20000, print_loss=True):
    #     pass



class Layer():
    def __init__(self, num_neurons, actFun_type):
        """
        :param num_neurons: Number of neurons in this layer
        :param actFun_type: type of activation function for this layer
        """
        self.num_neurons =num_neurons
        self.actFun_type =actFun_type
        self.input = []
        self.output = []
        self.z = []
        self.dR = []

    def feedforward(self, x, weights, bias):
        """
        :param x: input to this layer
        :param weights: weight matrix for this layer
        :param bias: bias term for this layer
        """
        self.input = x
        self.z = x.dot(weights) + bias
        self.output = actFun(self.z, type=self.actFun_type)
        return self.output


    def backprop(self, dR, weights_R):
        """
        :param delta_R: delta for (current layer + 1)
        :param weights_R: weight matrix between current layer and the right layer to it
        :return:
            dW: Derivative wrt input weights
            db: Derivative wrt bias
        """
        derivative = diff_actFun(self.z, self.actFun_type)
        self.delta = dR.dot(weights_R.T)*derivative
        dW = self.input.T.dot(self.delta)
        db = self.delta.sum(axis=0)
        return dW, db




    #n feedforward, backprop, calculate loss and fit model


def main():
    # generate and visualize Make-Moons dataset
    X, y = generate_data()
    # plt.scatter(X[:, 0], X[:, 1], s=40, c=y, cmap=plt.cm.Spectral)
    # plt.show()
    # model = DeepNeuralNetwork(nn_input_dim=2, nn_hidden_dim=3, nn_output_dim=2, actFun_type='relu')
    # model.actFun(5, 'relu')
    # print model.actFun(1, 'tanh')
    # print model.diff_actFun(1, 'tanh')
    # print model.feedforward(X, 'abd')
    # print model.calculate_loss(X, y)
    # print model.predict(X)
    # print model.backprop(X, y)
    # # print(help(NeuralNetwork))
    lyr = Layer(5, 'sigmoid')
    weights= np.random.rand(2,5)
    # print lyr.feedforward(X,weights, 0.1)
    delta_R = np.random.rand(200,5)
    weights_R = np.random.rand(5, 5)

    # print lyr.backprop(delta_R, weights_R)

    dnn=DeepNeuralNetwork(4, (2, 5, 4, 2), ['tanh', 'tanh', 'tanh', 'tanh'], reg_lambda=0.01, seed=0)
    #print dnn.weights, dnn.biases
    print dnn.feedforward(X)

    print len(dnn.weights)
    print dnn.backprop(X, y)

    # model = NeuralNetwork(nn_input_dim=2, nn_hidden_dim=3 , nn_output_dim=2, actFun_type='tanh')
    # model.fit_model(X, y)
    # model.visualize_decision_boundary(X, y)


if __name__ == "__main__":
    main()